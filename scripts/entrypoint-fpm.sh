#!/bin/bash

set -e

. /opt/bin/global.sh

su -s /bin/bash - www-data -c "cd /var/www/gallery && php artisan route:cache"

/usr/local/sbin/php-fpm
