#!/bin/bash

set -e

. /opt/bin/global.sh

su -s /bin/bash - www-data -c "cd /var/www/gallery && php artisan route:cache && php artisan queues enabled"

/usr/bin/supervisord -n -c /etc/supervisord.conf
