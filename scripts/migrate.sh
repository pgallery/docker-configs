#!/bin/bash

set -e

su -s /bin/bash - www-data -c "ln -s /opt/pgallery/.env /var/www/gallery/.env && \
    cd /var/www/gallery && php artisan migrate"

if [ $(/var/www/gallery/artisan user:check --role=admin) -eq 0 ]; then

    su -s /bin/bash - www-data -c "cd /var/www/gallery && php artisan db:seed"

    if [ -n ${GALLERY_USER} ] || [ -n ${GALLERY_PASSWORD} ]; then
        su -s /bin/bash - www-data -c "cd /var/www/gallery && php artisan usermod admin@example.com --email=${GALLERY_USER} --password=${GALLERY_PASSWORD}"
    fi

fi
